import xlrd
import faker
import random
import string
import pandas
import traceback
class ReadExcelData:
    def validate_the_sheet_in_ms_excel_file(self, filepath, sheetName):
        try:
            """Returns the True if the specified work sheets exist in the specifed MS Excel file else False"""
            workbook = xlrd.open_workbook(filepath)
            snames = workbook.sheet_names()
            sStatus = False
            if sheetName == None:
                return True
            else:
                for sname in snames:
                    if sname.lower() == sheetName.lower():
                        wsname = sname
                        sStatus = True
                        break
                if sStatus == False:
                    print("Error: The specified sheet: " + str(sheetName) + " doesn't exist in the specified file: " + str(
                        filepath))
            return sStatus
        except:
            traceback.print_exc()

    def get_ms_excel_row_values_into_dictionary_based_on_key(self, filepath, keyName, sheetName=None):
        try:
            """Returns the dictionary of values given row in the MS Excel file """
            workbook = xlrd.open_workbook(filepath)
            snames = workbook.sheet_names()
            dictVar = {}
            if sheetName == None:
                sheetName = snames[0]
            if self.validate_the_sheet_in_ms_excel_file(filepath, sheetName) == False:
                return dictVar
            worksheet = workbook.sheet_by_name(sheetName)
            noofrows = worksheet.nrows
            dictVar = {}
            headersList = worksheet.row_values(int(0))
            for rowNo in range(1, int(noofrows)):
                rowValues = worksheet.row_values(int(rowNo))
                if str(rowValues[0]) != str(keyName):
                    continue
                for rowIndex in range(0, len(rowValues)):
                    cell_data = rowValues[rowIndex]
                    if (str(cell_data) == "" or str(cell_data) == None):
                        continue
                    cell_data = self.get_unique_test_data(cell_data)
                    dictVar[str(headersList[rowIndex])] = str(cell_data)
            return dictVar
        except:
            traceback.print_exc()

    def get_unique_test_data(self, testdata):
        try:
            """Returns the unique if data contains unique word """
            letters = string.ascii_lowercase
            unique_string = ''.join(random.choice(letters) for i in range(0,4))
            unique_string = str(unique_string)
            if "UNIQUE" in testdata:
                testdata = testdata.replace("UNIQUE", unique_string)
            elif "Unique" in testdata:
                testdata = testdata.replace("Unique", unique_string)
            elif "unique" in testdata:
                testdata = testdata.replace("unique", unique_string)
            else:
                testdata = testdata
            return testdata
        except:
            traceback.print_exc()

    def readColumnDataPandas(self,filePath,SheetName,columnName):
        try:
            data = pandas.read_excel(filePath,sheet_name=SheetName)
            myList=data[columnName].tolist()
            return myList
        except:
            traceback.print_exc()


# obj = ReadExcelData()
# dirPath = pathlib.Path(__file__).parent.parent.joinpath("ExcelFile\Openings.xlsx")
# dict2 = obj.get_ms_excel_row_values_into_dictionary_based_on_key(dirPath,"login1","Credentials")
# # dict3 = obj.readColumnDataPandas(dirPath,"Emails")
# print(dict2)