import smtplib
import pathlib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import traceback
import pytest
from emailwithattachment.PythonPractise.SendMail.ReadExcelData import ReadExcelData

class TestSendMail(ReadExcelData):
    def test_sendMailForOpening(self):
        try:
            credentialsSheetName = "Credentials"
            EmailSheetName = "Emails"
            resumefileNmae = "Rohith_Resume_3Years11MonthsExp_In_SoftwareTesting.docx"
            # Getting path of the file
            ExceldirPath = pathlib.Path(__file__).parent.parent.joinpath("ExcelFile\Openings.xlsx")
            # Reading the data from excel
            mail_List = ReadExcelData.readColumnDataPandas(self, ExceldirPath, EmailSheetName, "Recruiter_Mail")
            print(mail_List)
            credentials_data = ReadExcelData.get_ms_excel_row_values_into_dictionary_based_on_key(self, ExceldirPath, "Key",
                                                                                              credentialsSheetName)
            # Creating an SMTP session for sending email
            sim = smtplib.SMTP('smtp.gmail.com', 587)
            # Enabling security
            sim.starttls()
            subject = credentials_data['Subject']
            body = u''.join(credentials_data['Body']).encode('ascii','ignore').decode('ascii')
            senderEmail = credentials_data['Login']
            pwd = credentials_data['Password']
            message  = MIMEMultipart()
            message["From"] = senderEmail
            message["Subject"] = subject
            message.attach(MIMEText(body,'plain'))
            ResumeFileName = pathlib.Path(__file__).parent.parent.joinpath("Resume\\"+resumefileNmae)
            with open(ResumeFileName,'rb') as attachment:
                mime = MIMEBase('application','octet-stream')
                mime.set_payload((attachment).read())
            encoders.encode_base64(mime)
            mime.add_header("Content-Disposition",f"attachment;filename = {resumefileNmae}",)
            message.attach(mime)
            text = message.as_string()
            sim.login(senderEmail,pwd)
            for i in range(0,len(mail_List)):
                sim.sendmail(senderEmail,mail_List[i],text)
            sim.quit()
        except:
            traceback.print_exc()